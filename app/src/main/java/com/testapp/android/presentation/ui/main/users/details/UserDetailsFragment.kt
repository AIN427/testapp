package com.testapp.android.presentation.ui.main.users.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.testapp.android.R
import com.testapp.android.di.module.GlideApp
import com.testapp.android.presentation.ui.main.users.UsersAdapter
import com.testapp.android.presentation.util.Constants.BUNDLE_USER
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_user_details.*
import pers.victor.ext.click
import javax.inject.Inject

class UserDetailsFragment : DaggerFragment(R.layout.fragment_user_details) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var adapter: UsersAdapter

    private val viewModel: UserDetailsViewModel by viewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        subscribeToData()

        viewModel.getUserDetails(arguments?.getLong(BUNDLE_USER, 0)!!)
    }


    private fun setupViews() {
        btn_back.click {
            findNavController().popBackStack()
        }
    }

    private fun subscribeToData() {
        viewModel.userDetailsLiveData.observe(viewLifecycleOwner, {
            text_fio.text = getString(R.string.user_fio, it.firstName, it.lastName)
            text_email.text = it.email

            GlideApp.with(requireContext())
                .load(it.avatar)
                .transform(CenterCrop(), RoundedCorners(16))
                .into(image_user_avatar)

        })
    }
}