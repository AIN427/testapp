package com.testapp.android.presentation.ui.base

enum class ViewResultType(var error: String?) {
    PROGRESS(null), PLACEHOLDER(null), DATA(null), ERROR(null)
}