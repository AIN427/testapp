package com.testapp.android.presentation.ui.main.users

import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.testapp.android.R
import com.testapp.android.data.local.entity.UserEntity
import com.testapp.android.di.module.GlideApp
import javax.inject.Inject

class UsersAdapter @Inject constructor() : BaseQuickAdapter<UserEntity, BaseViewHolder>(R.layout.item_user) {

    override fun convert(holder: BaseViewHolder, item: UserEntity) {
        holder.setText(R.id.text_fio, context.getString(R.string.user_fio, item.firstName, item.lastName))
                .setText(R.id.text_email, item.email)

        GlideApp.with(context)
                .load(item.avatar)
                .transform(CenterCrop(), RoundedCorners(16))
                .into(holder.getView(R.id.image_user_avatar))
    }
}