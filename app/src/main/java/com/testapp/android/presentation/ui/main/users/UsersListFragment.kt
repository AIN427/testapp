package com.testapp.android.presentation.ui.main.users

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.testapp.android.R
import com.testapp.android.presentation.ui.base.ViewResultType
import com.testapp.android.presentation.util.Constants.BUNDLE_USER
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_list_users.*
import pers.victor.ext.gone
import pers.victor.ext.toast
import pers.victor.ext.visiable
import javax.inject.Inject


class UsersListFragment : DaggerFragment(R.layout.fragment_list_users) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var adapter: UsersAdapter

    private val viewModel: UsersListViewModel by viewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        subscribeToData()

        viewModel.loadUsers()
    }

    private fun setupViews() {
        list_users.adapter = adapter
        adapter.setOnItemClickListener { _, view, position ->
            val item = adapter.getItem(position)

            val bundle = Bundle()
            bundle.putLong(BUNDLE_USER, item.id!!)

            findNavController().navigate(R.id.item_users_details, bundle)
        }
    }

    private fun subscribeToData() {
        viewModel.usersLiveData.observe(viewLifecycleOwner, {
            adapter.setNewInstance(it.result)
        })

        viewModel.viewResultType.observe(viewLifecycleOwner, { viewTypeResult ->
            progress_layout.gone()
            list_users.gone()

            when (viewTypeResult) {
                ViewResultType.PLACEHOLDER -> {
                }
                ViewResultType.PROGRESS -> {
                    progress_layout.visiable()
                }
                ViewResultType.DATA -> {
                    list_users.visiable()
                }
                ViewResultType.ERROR -> {
                    toast("error")
                }
            }
        })
    }
}