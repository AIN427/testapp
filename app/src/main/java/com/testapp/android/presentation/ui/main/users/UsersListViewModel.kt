package com.testapp.android.presentation.ui.main.users

import androidx.lifecycle.MutableLiveData
import com.testapp.android.data.local.entity.UsersListEntity
import com.testapp.android.domain.usecase.base.DefaultInternetSubscriber
import com.testapp.android.domain.usecase.users.GetAllUsersUseCase
import com.testapp.android.presentation.ui.base.BaseViewModel
import com.testapp.android.presentation.ui.base.ViewResultType
import javax.inject.Inject

class UsersListViewModel @Inject constructor(private val getAllUsersUseCase: GetAllUsersUseCase) : BaseViewModel() {
    val usersLiveData = MutableLiveData<UsersListEntity>()
    val viewResultType = MutableLiveData<ViewResultType>()

    fun loadUsers() {
        addDisposable(getAllUsersUseCase.execute(object : DefaultInternetSubscriber<UsersListEntity>() {
            override fun onNext(it: UsersListEntity) {
                usersLiveData.value = it

                viewResultType.value = ViewResultType.DATA
            }

            override fun onError(throwable: Throwable) {
                super.onError(throwable)

                viewResultType.value = ViewResultType.ERROR

            }
        }))
    }
}