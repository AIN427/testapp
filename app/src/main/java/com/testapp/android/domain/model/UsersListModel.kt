package com.testapp.android.domain.model

import com.google.gson.annotations.SerializedName

class UsersListModel {
    @SerializedName("data")
    val data: List<UserModel>? = null

    @SerializedName("total")
    val total: Long? = 0

    @SerializedName("total_pages")
    val totalPages: Long? = 0
}