package com.testapp.android.domain.repository

import com.testapp.android.data.local.entity.UserEntity
import com.testapp.android.data.local.entity.UsersListEntity
import io.reactivex.rxjava3.core.Observable

interface UsersRepository {
    fun getAllUsers(): Observable<UsersListEntity>
    fun getUserDetails(userId: Long?): Observable<UserEntity>
}