package com.testapp.android.domain.usecase.users

import com.testapp.android.data.local.entity.UserEntity
import com.testapp.android.domain.repository.UsersRepository
import com.testapp.android.domain.usecase.base.BackgroundUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetUserDetailsUseCase @Inject constructor(private val usersRepository: UsersRepository) : BackgroundUseCase() {

    var userId: Long? = null

    override fun buildObservableTask(): Observable<UserEntity> {
        return usersRepository.getUserDetails(userId)
    }
}