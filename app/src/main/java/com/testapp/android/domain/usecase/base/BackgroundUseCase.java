package com.testapp.android.domain.usecase.base;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public abstract class BackgroundUseCase extends UseCase {

    @Override
    public DisposableObserver execute(DisposableObserver observableTaskSubscriber) {
        final Observable requestObservable;
        if ((requestObservable = buildObservableTask()) == null)
            throw new RuntimeException("Error observable request not declared");
        final Observable observable = requestObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        return (DisposableObserver) observable.subscribeWith(observableTaskSubscriber);
    }
}
