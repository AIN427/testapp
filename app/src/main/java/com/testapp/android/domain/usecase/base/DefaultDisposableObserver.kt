package com.testapp.android.domain.usecase.base

import io.reactivex.rxjava3.observers.DisposableObserver

abstract class DefaultDisposableObserver<T> : DisposableObserver<T>() {
    override fun onNext(t: T) {}
    override fun onError(e: Throwable) {
    }

    override fun onComplete() {}
}