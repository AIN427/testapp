package com.testapp.android.domain.usecase.base

import androidx.annotation.CallSuper

abstract class DefaultInternetSubscriber<R> : DefaultDisposableObserver<R>() {

    @CallSuper
    override fun onError(throwable: Throwable) {
    }

}