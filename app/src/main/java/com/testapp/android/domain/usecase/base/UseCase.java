package com.testapp.android.domain.usecase.base;


import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.observers.DisposableObserver;

public abstract class UseCase<T extends DisposableObserver> {

    protected abstract Observable buildObservableTask();

    public Observer execute(final T observableTaskSubscriber) {
        return buildObservableTask().subscribeWith(observableTaskSubscriber);
    }
}