package com.testapp.android.domain.model

import com.google.gson.annotations.SerializedName

class UserDetailsModel {
    @SerializedName("data")
    var data: UserModel? = null
}