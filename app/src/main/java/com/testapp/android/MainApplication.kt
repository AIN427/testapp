package com.testapp.android

import android.content.Context
import androidx.multidex.MultiDex
import com.testapp.android.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import pers.victor.ext.Ext

class MainApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        Ext.with(this)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

    private val applicationInjector = DaggerAppComponent.factory().create(this)
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationInjector
}