package com.testapp.android.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.testapp.android.data.local.dao.UserDao
import com.testapp.android.data.local.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val userDao: UserDao

    companion object {
        const val DB_NAME = "sofjet.db"
    }
}