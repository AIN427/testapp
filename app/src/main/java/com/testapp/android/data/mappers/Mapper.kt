package com.testapp.android.data.mappers


abstract class Mapper<in E, T> {
    abstract fun mapFrom(from: E): T
}