package com.testapp.android.data.mappers

import com.testapp.android.data.local.entity.UserEntity
import com.testapp.android.data.local.entity.UsersListEntity
import com.testapp.android.domain.model.UsersListModel
import javax.inject.Inject

class UsersListToEntityMapper @Inject constructor() : Mapper<UsersListModel, UsersListEntity>() {
    override fun mapFrom(from: UsersListModel): UsersListEntity {
        val usersListEntity = UsersListEntity()
        usersListEntity.total = from.total
        usersListEntity.totalPages = from.totalPages
        usersListEntity.result = from.data?.map { model ->
            return@map UserEntity(
                    id = model.id,
                    email = model.email,
                    firstName = model.firstName,
                    lastName = model.lastName,
                    avatar = model.avatar
            )
        }?.toMutableList()
        return usersListEntity
    }
}