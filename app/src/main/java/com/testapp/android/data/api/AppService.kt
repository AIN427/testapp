package com.testapp.android.data.api

import com.testapp.android.domain.model.UserDetailsModel
import com.testapp.android.domain.model.UsersListModel
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface AppService {

    @GET("users")
    fun getUsers(): Observable<UsersListModel>

    @GET("users/{id}")
    fun getUserDetails(@Path("id") userId: Long?): Observable<UserDetailsModel>
}