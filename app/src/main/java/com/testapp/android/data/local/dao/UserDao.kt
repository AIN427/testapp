package com.testapp.android.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testapp.android.data.local.entity.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userEntity: UserEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllUsers(userEntity: MutableList<UserEntity>)

    @Query("SELECT * FROM Users")
    fun loadUsers(): MutableList<UserEntity>
}