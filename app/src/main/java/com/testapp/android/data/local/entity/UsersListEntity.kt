package com.testapp.android.data.local.entity

class UsersListEntity {
    var result: MutableList<UserEntity>? = null
    var total: Long? = 0
    var totalPages: Long? = 0
}