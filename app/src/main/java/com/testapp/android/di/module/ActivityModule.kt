package com.testapp.android.di.module

import com.testapp.android.di.scope.PerActivity
import com.testapp.android.presentation.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @PerActivity
    @ContributesAndroidInjector
    abstract fun get(): MainActivity
}
