package com.testapp.android.di.module

import com.testapp.android.data.api.AppService
import com.testapp.android.data.mappers.UserDetailsToEntityMapper
import com.testapp.android.data.mappers.UsersListToEntityMapper
import com.testapp.android.data.repository.UsersRepositoryImpl
import com.testapp.android.domain.repository.UsersRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DataModule {

    @Singleton
    @Provides
    fun providesUsersRepository(
        appService: AppService,
        usersListToToEntityMapper: UsersListToEntityMapper,
        usersDetailsToEntityMapper: UserDetailsToEntityMapper
    ): UsersRepository = UsersRepositoryImpl(appService, usersListToToEntityMapper, usersDetailsToEntityMapper)
}