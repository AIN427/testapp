package com.testapp.android.di.component

import com.testapp.android.MainApplication
import com.testapp.android.di.module.*
import com.testapp.android.di.module.viewModuile.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class), (ApplicationModule::class), (ActivityModule::class), (FragmentModule::class), (NetworkModule::class), (DatabaseModule::class), (ViewModelModule::class), (DataModule::class)])
interface AppComponent : AndroidInjector<MainApplication> {
    @Component.Factory
    interface Factory : AndroidInjector.Factory<MainApplication>
}
