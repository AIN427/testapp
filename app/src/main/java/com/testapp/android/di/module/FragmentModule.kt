package com.testapp.android.di.module

import com.testapp.android.di.scope.PerFragment
import com.testapp.android.presentation.ui.main.about.AboutFragment
import com.testapp.android.presentation.ui.main.users.UsersListFragment
import com.testapp.android.presentation.ui.main.users.details.UserDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    abstract fun usersListFragment(): UsersListFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun aboutFragment(): AboutFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun userDetailsFragment(): UserDetailsFragment
}
