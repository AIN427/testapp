package com.testapp.android.di.module

import android.content.Context
import com.testapp.android.MainApplication
import com.testapp.android.di.qualifier.ApplicationContext
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    @ApplicationContext
    internal abstract fun application(mainApplication: MainApplication): Context
}
