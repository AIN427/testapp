package com.testapp.android.di.module

import com.testapp.android.di.module.viewModuile.ViewModelModule
import dagger.Module

@Module(
    includes = [
        ViewModelModule::class,
        NetworkModule::class
    ]
)
class AppModule